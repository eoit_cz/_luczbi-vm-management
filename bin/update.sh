#! /bin/bash
find . -type d -name "__pycache__" -exec rm -rf "{}" \; > /dev/null
rm -f ./log/acces.log ./log/error.log
pip install virtualenv
virtualenv dev
source ./dev/bin/activate
pip install -r requirements.txt
