from azure.common.credentials import ServicePrincipalCredentials

_token = b''  # nech prazdne generuje se za běhu
subscription_id = "47f2e9b3-a70e-406b-bb0c-bcb42a95bcd2"
credentials = ServicePrincipalCredentials(
    client_id="3ef0dc87-d332-4439-a65f-161c1fcacd36",
    secret="",
    tenant="03e6c03e-074e-474c-8d40-3eac96d82a77"
)
rg_name = "rg-bicz-utils"
vm_names = [
    "vm-bicz-work1",
    "vm-bicz-work2",
    "vm-bicz-work3",
    "vm-bicz-work4",
    "vm-bicz-work5",
    "vm-bicz-work6",
    "vm-bicz-work7",
    "vm-bicz-work8"
]
vm_username = "cassandra"
vm_password = ""
winrm_operation_timeout_sec = 10
winrm_read_timeout_sec = 15
api_cache_timeout_sec = 20
watchdog_loop_time_sec = 30
app_log_path = 'log/app_info.log'
app_log_format = '%(asctime)s;%(message)s'