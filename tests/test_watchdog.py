import unittest
import src.watchdog as watchdog

class TestWatchdog(unittest.TestCase):
    
    def test_group_by_vm_status(self):
        self.assertTrue(callable(watchdog.group_by_vm_status))
        before = {
                'vm-bicz-work5':
                    {'status': 'running', 'user': 'unknown',
                    'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work7'},
                'vm-bicz-work6':
                    {'status': 'deallocated', 'user': '',
                        'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work7'},
                'vm-bicz-work7':
                    {'status': 'running', 'user': '',
                        'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work7'},
                'vm-bicz-work8':
                    {'status': 'deallocated', 'user': '',
                        'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work8'}
        }

        after = {'running':
                 [
                     {'status': 'running', 'user': 'unknown',
                      'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work7'},
                     {'status': 'running', 'user': '',
                      'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work7'}
                 ],
                 'deallocated':
                 [
                     {'status': 'deallocated', 'user': '',
                      'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work7'},
                     {'status': 'deallocated', 'user': '',
                      'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work8'}
                 ]
                }
        result = watchdog.group_by_vm_status(before)
        self.assertEqual(result, after)

    def test_find_changed(self):
        self.assertTrue(callable(watchdog.find_changed))
        prev = [
                 {'status': 'running', 'user': '',
                    'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work7'}
                ]
        curr = [
                 {'status': 'running', 'user': '',
                    'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work7'},
                {'status': 'running', 'user': '',
                    'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work8'},
                ]
        changed = [
                 {'status': 'running', 'user': '',
                    'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work8'},
                ]
        result = watchdog.find_changed(None, curr)
        self.assertEqual(result, [])
        result = watchdog.find_changed(prev, curr)
        self.assertEqual(result, changed)
    
    def test_find_all_changed(self):
        self.assertTrue(callable(watchdog.find_all_changed))
        prev = {'running':
                 [
                     {'status': 'running', 'user': 'unknown',
                      'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
                     {'status': 'running', 'user': 'A12345',
                      'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'}
                 ],
                 'deallocated':
                 [
                     {'status': 'deallocated', 'user': '',
                      'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                     {'status': 'deallocated', 'user': '',
                      'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'}
                 ]
                }
        curr = {'running':
                 [
                     {'status': 'running', 'user': 'unknown',
                      'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
                     {'status': 'running', 'user': 'A12345',
                      'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                      {'status': 'running', 'user': 'B54321',
                      'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'}
                 ],
                 'deallocated':
                 [
                     {'status': 'deallocated', 'user': '',
                      'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                    {'status': 'deallocated', 'user': '',
                      'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'}
                 ]
                }
        changed = [
                     {'status': 'running', 'user': 'B54321',
                      'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                     {'status': 'deallocated', 'user': '',
                      'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'}
                 ]
        result = watchdog.find_all_changed(None, curr)
        self.assertEqual(result, [])
        result = watchdog.find_all_changed(prev, curr)
        self.assertEqual(result, changed)
    
    def test_group_by_availability(self):
        self.assertTrue(callable(watchdog.group_by_availability))
        before = {
            'vm-bicz-work1':
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
            'vm-bicz-work2':
                {'status': 'starting', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
            'vm-bicz-work3':
                {'status': 'running', 'user': 'unknown', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
            'vm-bicz-work4':
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
            'vm-bicz-work5':
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
            'vm-bicz-work6':
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
            'vm-bicz-work7':
                {'status': 'deallocating', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
            'vm-bicz-work8':
                {'status': 'unknown', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
        }

        after = {
            'available': [
                {'status': 'starting', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'running', 'user': 'unknown', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'}
            ],
            'unavailable': [
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'deallocating', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'unknown', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ],
            'in_use': [
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'}
            ]
        }
        result = watchdog.group_by_availability({})
        self.assertEqual(result, {})
        result = watchdog.group_by_availability(before)
        self.assertEqual(result, after)

    def test_manage_vms_keep_one_running(self):
        self.maxDiff = None
        self.assertTrue(callable(watchdog.manage_vms_keep_one_running))
        result = watchdog.manage_vms_keep_one_running({})
        self.assertEqual(result, {})
        # Case: All are in use
        status = {
            'in_use': [
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'occupied', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ]
        }
        actions = {}
        result = watchdog.manage_vms_keep_one_running(status)
        self.assertEqual(result, actions)

        # Case: All VMs are unavailable
        status = {
            'unavailable': [
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'deallocating', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ]
        }
        actions = {
            'activate': [
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'}
            ]
        }
        result = watchdog.manage_vms_keep_one_running(status)
        self.assertEqual(result, actions)
        
        # Case: More than one VM are available 
        status = {
            'available': [
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ]
        }
        actions = {
            'deactivate': [
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ]
        }
        result = watchdog.manage_vms_keep_one_running(status)
        self.assertEqual(result, actions)

        # Case: Some VMs are stopped 
        status = {
            'available': [
                {'status': 'running', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
            ],
            'unavailable': [
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ]
        }
        actions = {
            'deactivate': [
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'}
            ]
        }
        result = watchdog.manage_vms_keep_one_running(status)
        self.assertEqual(result, actions)

        # Case: Stopped VMS should be activated 
        status = {
            'unavailable': [
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ]
        }
        actions = {
            'activate': [
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'}
            ],
            'deactivate': [
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'stopped', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ]
        }
        result = watchdog.manage_vms_keep_one_running(status)
        self.assertEqual(result, actions)

        # Case: All VMs are unavailable
        status = {
            'unavailable': [
                {'status': 'deallocating', 'user': '', 'vm_ip': '10.76.65.121', 'vm_name': 'vm-bicz-work1'},
                {'status': 'deallocating', 'user': '', 'vm_ip': '10.76.65.122', 'vm_name': 'vm-bicz-work2'},
                {'status': 'deallocating', 'user': '', 'vm_ip': '10.76.65.123', 'vm_name': 'vm-bicz-work3'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.125', 'vm_name': 'vm-bicz-work5'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.126', 'vm_name': 'vm-bicz-work6'},
                {'status': 'deallocating', 'user': '', 'vm_ip': '10.76.65.127', 'vm_name': 'vm-bicz-work7'},
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.128', 'vm_name': 'vm-bicz-work8'}
            ]
        }
        actions = {
            'activate': [
                {'status': 'deallocated', 'user': '', 'vm_ip': '10.76.65.124', 'vm_name': 'vm-bicz-work4'},
            ]
        }
        result = watchdog.manage_vms_keep_one_running(status)
        self.assertEqual(result, actions)

if __name__ == '__main__':
    unittest.main()
