import sys
import os
import asyncio
import functools
import signal
import logging

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
import config as cfg
from lib.vminspector import VMInspector

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.FileHandler(cfg.app_log_path)
handler.setLevel(logging.INFO)
formatter = logging.Formatter(cfg.app_log_format)
handler.setFormatter(formatter)
logger.addHandler(handler)

# logger2 = logging.getLogger('Simulator')
# logger2.setLevel(logging.INFO)
# handler = logging.FileHandler('log/app_simulator.log')
# handler.setLevel(logging.INFO)
# formatter = logging.Formatter(cfg.app_log_format)
# handler.setFormatter(formatter)
# logger2.addHandler(handler)

def add_to_group_by_name(result, group_name, item):
    group = result.get(group_name, [])
    if type(item) is list:
        group += item
    else:
        group.append(item)
        
    result[group_name] = group

def group_by_vm_status(vm_list):
    """Re-sort vm_list by status"""
    result = {}
    for vm in vm_list.values():
        add_to_group_by_name(result, vm['status'], vm)
    return result
    
def group_by_availability(vm_list):
    """Re-sort vm_list by availability groups available/unavailable/in use"""
    result = {}
    for vm in vm_list.values():
        if vm['status'] == 'starting' or (vm['status'] == 'running' and (vm['user'] == '' or vm['user'] == 'unknown')):
            add_to_group_by_name(result, 'available', vm)
        elif vm['status'] == 'occupied':
            add_to_group_by_name(result, 'in_use', vm)
        else:
            add_to_group_by_name(result, 'unavailable', vm)
    return result

def find_changed(prev, curr):
    """Find change in VMs status to be logged"""
    result = []
    if prev == None:
        return result
    for i in curr:
        if i not in prev:
            result.append(i)
    return result

def find_all_changed(prev, curr):
    """Find change across all VMs statuses to be logged"""
    result = []
    if prev == None:
        return result
    for status in curr:
        result += find_changed(prev.get(status,[]), curr.get(status,[]))
    return result

def deactivate(vms,vmi):
    """Deactivate VM"""
    for vm in vms:
        # logger2.info("Deactivating;{};{};{}".format(vm['vm_name'], vm['status'], vm['user']))
        vmi.DeallocateVM(vm['vm_name'])

def activate(vms,vmi):
    """Activate VM"""
    for vm in vms:
        # logger2.info("Activating;{};{};{}".format(vm['vm_name'], vm['status'], vm['user']))
        vmi.StartVM(vm['vm_name'])

def log_status_change(changed):
    for vm in changed:
        logger.info("{};{};{}".format(vm['vm_name'], vm['status'], vm['user']))

def manage_vms_keep_one_running(vms):
    result = {}
    if vms == {}:
        return result
    # Need at least one VM to be Activated
    if (('available' not in vms or len(vms['available']) == 0) and 
        ('unavailable' in vms and len(vms['unavailable']) > 0)):
        for vm in vms['unavailable']:
            if vm['status'] != 'deallocating':
                add_to_group_by_name(result, 'activate', vm)
                break
    # Need only one VM to be active
    if ('available' in vms and len(vms['available']) > 1):
        add_to_group_by_name(result, 'deactivate', vms['available'][1:])
    # Deallocate Stopped VMS unless they should not be Activated
    if ('unavailable' in vms and len(vms['unavailable']) > 0):
        for vm in vms['unavailable']:
            if vm['status'] == 'stopped':
                if 'activate' in result and vm in result['activate']:
                    continue
                add_to_group_by_name(result, 'deactivate', vm)
    return result

def check_vms(loop, interval, vmi, prev_status):
    """Check and manage VM status via VMInspector"""
    vms_status = vmi.inspectAll()

    curr_status = group_by_vm_status(vms_status)
    log_status_change(find_all_changed(prev_status, curr_status))

    vms_by_availability = group_by_availability(vms_status)
    actions = manage_vms_keep_one_running(vms_by_availability)

    if 'activate' in actions:
            activate(actions['activate'],vmi)
    if 'deactivate' in actions:
            deactivate(actions['deactivate'],vmi)

    loop.call_later(interval, check_vms, loop, interval, vmi, curr_status)

def ask_exit(signame):
    """Hook signals for teminations"""
    # print("Recieved signal %s: exiting…" % signame)
    loop.stop()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    interval = cfg.watchdog_loop_time_sec
    vmi = VMInspector()
    prev_status = None
    loop.call_soon(check_vms, loop, interval, vmi, prev_status)

    for signame in ('SIGINT', 'SIGTERM'):
        loop.add_signal_handler(getattr(signal, signame), functools.partial(ask_exit, signame))
    print("Event loop running forever, press Ctrl+C to interrupt.")
    print("pid %s: send SIGINT or SIGTERM to exit." % os.getpid())
    try:
        loop.run_forever()
    finally:
        loop.close()