import falcon
import config as cfg
from lib.vminspector import VMInspector
from threading import Event, Thread


class VMStatusResource():
    def __init__(self):
        self.vm_status = None
        self.vminspector = VMInspector()
        self.call_repeatedly(cfg.api_cache_timeout_sec, self.fetch_status)

    def call_repeatedly(self, interval, func, *args):
        """Run func callback in defined interval in separated thread"""
        stopped = Event()
        def loop():
            while not stopped.wait(interval): 
                func(*args)
        func(*args) # Run now then start the job
        Thread(target=loop, daemon=True).start()
        return stopped.set

    def fetch_status(self):
        """Updates status var of the VMs"""
        self.vm_status = self.vminspector.inspectAll()
    
    def update_status(self, vm, status):
        self.vm_status[vm]['status'] = status

    def on_get(self, req, resp):
        """Return VM Status as JSON"""
        resp.media = self.vm_status


class DeallocateVMResource():
    def __init__(self, vmsr):
        self.vmsr = vmsr
        self.vm_inspector = VMInspector()

    def on_get(self, req, resp, vm_name):
        """Deallocate VM"""
        try:
            status = self.vm_inspector.DeallocateVM(vm_name)
            vmsr.update_status(vm_name, status)
            resp.media = {
                "status" : status,
                "msg": "Deallocating: " + vm_name
            }
        except:
            resp.status = falcon.HTTP_500
            resp.media = {
                "status" : "error",
                "msg": "Invalid VM name: " + vm_name
            }


class StartVMResource():
    def __init__(self, vmsr):
        self.vmsr = vmsr
        self.vm_inspector = VMInspector()

    def on_get(self, req, resp, vm_name):
        """Start VM"""
        try:
            status = self.vm_inspector.StartVM(vm_name)
            vmsr.update_status(vm_name, status)
            resp.media = {
                "status" : status,
                "msg": "Starting: " + vm_name
            }
        except:
            resp.status = falcon.HTTP_500
            resp.media = {
                "status" : "error",
                "msg": "Invalid VM name: " + vm_name
            }

class RDPResource():
    def __init__(self, vmsr):
        self.vmsr = vmsr
    def on_get(self, req, resp, vm_name):
        """Return RDP file"""
        vm = self.vmsr.vm_status.get(vm_name, None)
        if vm:
            content = ("full address:s:" + vm['vm_ip'] + ":3389\nprompt for credentials:i:1\nadministrative session:i:1").encode('utf-8')
            filename = vm_name + ".rdp"
            resp.data = content
            resp.set_header("Content-Disposition", "attachment; filename=\"%s\"" % filename)
            resp.content_type = "application/rdp; charset=utf-8"
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_500
            resp.media = {
                "status" : "error",
                "msg": "Invalid VM name: " + vm_name
            }


app = falcon.API()
vmsr = VMStatusResource()
app.add_route('/', vmsr)
app.add_route('/deallocate/{vm_name}', DeallocateVMResource(vmsr))
app.add_route('/start/{vm_name}', StartVMResource(vmsr))
app.add_route('/rdp/{vm_name}', RDPResource(vmsr))
