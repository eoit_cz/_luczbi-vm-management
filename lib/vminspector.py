from azure.mgmt.compute import ComputeManagementClient
from azure.mgmt.network import NetworkManagementClient
from pprint import pprint
import config
import winrm
import os
from enum import Enum
import multiprocessing as mp
from hashlib import pbkdf2_hmac
import binascii
import sys
from pprint import pprint

VM_STATES = {
    "RUNNING": "running",
    "OCCUPIED": "occupied",
    "DEALLOCATING": "deallocating",
    "DEALLOCATED": "deallocated",
    "STARTING": "starting",
    "STOPPED": "stopped",
    "UNKNOWN": "unknown",
    "WAITING_4_CHANGE": "…"
}

class VMInspector:
    USER_ACTIVE = "Active"
    USER_UNKNOWN = "unknown"
    def __init__(self):
        self.compute_client = ComputeManagementClient(config.credentials, config.subscription_id)
        self.network_client = NetworkManagementClient(config.credentials, config.subscription_id)

    def inspect(self, vm_name):
        global VM_STATES
        vm = self.compute_client.virtual_machines.get(config.rg_name, vm_name, 'instanceView')
        vm_ip = self.getIp(vm)
        if len(vm.instance_view.statuses) > 1:
            status = vm.instance_view.statuses[1].code.replace("PowerState/", "")
        else:
            status = VM_STATES.get("UNKNOWN")
        vm_state = {}
        vm_state["vm_name"] = vm_name 
        vm_state["vm_ip"] = vm_ip
        # passwd = (vm_name + vm_ip).encode('utf-8') #make it binary string
        # dk = pbkdf2_hmac('sha256', passwd, config._token, 1) #_token is generated in __init__
        # vm_state["token"] = binascii.hexlify(dk).decode('utf-8') #make it plain string again
        vm_state["status"] = status
        if status == VM_STATES.get("RUNNING"):
            vm_state["user"] = self.getUser(vm_ip)
            if vm_state["user"] != "" and vm_state["user"] != self.USER_UNKNOWN:
                vm_state["status"] = VM_STATES.get("OCCUPIED")
        else:
            vm_state["user"] = ""
        return vm_state

    def inspectAll(self):
        # pool = mp.Pool(processes=mp.cpu_count())
        # vm_states_l = pool.map(self.inspect, config.vm_names)
        vm_states_l = list(map(self.inspect, config.vm_names))
        vm_states_d = {}
        for vm in vm_states_l:
            vm_states_d[vm['vm_name']] = vm
        return vm_states_d

    def getIp(self, vm):
        vm_interface = vm.network_profile.network_interfaces[0]
        name=" ".join(vm_interface.id.split('/')[-1:])
        sub="".join(vm_interface.id.split('/')[4])
        nic = self.network_client.network_interfaces.get(sub, name)
        return nic.ip_configurations[0].private_ip_address

    def getUser(self, ip):
        os.environ['NO_PROXY'] = ip # Hack if you are behind Proxy
        session = winrm.Session(
            target = 'https://' + ip + ':5986/wsman', 
            auth = (config.vm_username, config.vm_password), 
            transport = 'ntlm',
            operation_timeout_sec = config.winrm_operation_timeout_sec, 
            read_timeout_sec = config.winrm_read_timeout_sec,
            server_cert_validation='ignore'
        )
        try:
            result = session.run_cmd('query user 2>null').std_out.decode('utf-8').splitlines()
            for line in result:
                if self.USER_ACTIVE in line:
                    return line.split()[0]
            return ""
        except:
            e = sys.exc_info()
            pprint(e)
            return self.USER_UNKNOWN

    def DeallocateVM(self, vm_name):
        self.compute_client.virtual_machines.deallocate(config.rg_name, vm_name)
        return VM_STATES.get("WAITING_4_CHANGE")
        
    def StartVM(self, vm_name):
        self.compute_client.virtual_machines.start(config.rg_name, vm_name)
        return VM_STATES.get("WAITING_4_CHANGE")